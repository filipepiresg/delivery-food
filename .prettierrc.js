module.exports = {
  // bracketSpacing: false,
  jsxBracketSameLine: true,
  // trailingComma: 'all',
  printWidth: 100,
  singleQuote: true,
  jsxSingleQuote: true,
  semi: true,
  trailingComma: 'es5',
};
