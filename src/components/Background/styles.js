import styled from 'styled-components/native';
import LinearGradient from 'react-native-linear-gradient';
import { colors } from '~/styles';

export const Container = styled(LinearGradient).attrs({
  colors: [colors.RED_PRIMARY, colors.RED_SECONDARY],
  start: { x: 0, y: 0 },
  end: { x: 0, y: 1 },
  locations: [0.6, 1],
})`
  flex: 1;
`;
