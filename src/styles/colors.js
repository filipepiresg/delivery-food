export default {
  WHITE: '#fff',
  BLACK: '#000',
  RED_PRIMARY: '#B21F66',
  RED_SECONDARY: '#FE346E',
  TRANSPARENT: 'transparent',
  WARN: '#f0bc5e',
  // RED_PRIMARY: '#AF460F',
  // RED_SECONDARY: '#FE8761',
};
