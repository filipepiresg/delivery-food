import * as UserTypes from './types';

/**
 *
 * @param {Object} data
 * @param {!String} data.name
 * @param {!String} data.email
 * @param {!String} data.password
 * @param {!String} data.confirmPassword
 * @param {!String} data.oldPassword
 * @param {!Object} data.picture
 */
export function updateProfileRequest(data) {
  return {
    type: UserTypes.UPDATE_USER_REQUEST,
    payload: {
      ...data,
    },
  };
}

/**
 *
 * @param {Object} data
 * @param {Object} data.user
 * @param {String} data.user.name
 * @param {String} data.user.email
 * @param {Object} data.user.picture
 */
export function updateProfileSuccess(data) {
  return {
    type: UserTypes.UPDATE_USER_SUCCESS,
    payload: {
      ...data,
    },
  };
}

export function updateProfileFailure(error) {
  return {
    type: UserTypes.UPDATE_USER_FAILURE,
    payload: {
      error,
    },
  };
}
