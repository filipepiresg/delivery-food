import { Alert } from 'react-native';
import { all, call, put, select, takeLatest } from 'redux-saga/effects';

import api, { Endpoints } from '~/services/api';
import * as UserTypes from './types';
import { updateProfileFailure, updateProfileSuccess } from './actions';

export function* updateUser({ payload }) {
  try {
    const { id } = select(state => state.user.profile);

    const response = yield call(
      api.patch,
      `${Endpoints.USER}/${id}`,
      { ...payload },
      {
        timeout: 30000,
        headers: {
          'Content-Type': 'multipart/form-data',
        },
        mimeType: 'multipart/form-data',
      }
    );

    yield put(updateProfileSuccess(response.data));
  } catch (error) {
    Alert.alert('Ocorreu um erro');

    yield put(updateProfileFailure(error));
  }
}

export default all([takeLatest(UserTypes.UPDATE_USER_REQUEST, updateUser)]);
