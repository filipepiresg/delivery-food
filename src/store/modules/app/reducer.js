import produce from 'immer';

import * as AuthTypes from '../auth/types';
import * as UserTypes from '../user/types';

const INITIAL_STATE = {
  loading: false,
};

export default function app(state = INITIAL_STATE, action) {
  return produce(state, draft => {
    switch (action.type) {
      case UserTypes.GET_USER_REQUEST:
      case UserTypes.UPDATE_USER_REQUEST:
      case AuthTypes.SIGN_IN_REQUEST:
      case AuthTypes.SIGN_UP_REQUEST: {
        draft.loading = true;
        break;
      }
      case UserTypes.GET_USER_SUCCESS:
      case UserTypes.GET_USER_FAILURE:
      case UserTypes.UPDATE_USER_SUCCESS:
      case UserTypes.UPDATE_USER_FAILURE:
      case AuthTypes.SIGN_UP_SUCCESS:
      case AuthTypes.SIGN_UP_FAILURE:
      case AuthTypes.SIGN_IN_SUCCESS:
      case AuthTypes.SIGN_IN_FAILURE: {
        draft.loading = false;
        break;
      }
      default:
    }
  });
}
