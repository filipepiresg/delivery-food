import produce from 'immer';

import * as AuthTypes from './types';

const INITIAL_STATE = {
  token: null,
  signed: false,
};

export default function auth(state = INITIAL_STATE, action) {
  return produce(state, draft => {
    switch (action.type) {
      case AuthTypes.SIGN_IN_SUCCESS: {
        draft.signed = true;
        draft.token = action.payload.token;
        break;
      }
      case AuthTypes.SIGN_UP_SUCCESS: {
        draft.signed = true;
        draft.token = action.payload.token;
        break;
      }
      case AuthTypes.LOGOUT: {
        draft.signed = false;
        draft.token = null;
        break;
      }
      default:
    }
  });
}
