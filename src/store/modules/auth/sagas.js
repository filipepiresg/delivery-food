import { Alert } from 'react-native';
import auth from '@react-native-firebase/auth';
import firestore from '@react-native-firebase/firestore';
import storage from '@react-native-firebase/storage';
import { all, takeLatest, put } from 'redux-saga/effects';
import get from 'lodash/get';

import { Constants } from '~/commom';
import api from '~/services/api';
import * as AuthTypes from './types';
import { signInSuccess, signUpSuccess, signUpFailure, signInFailure, logout } from './actions';

export function* signIn({ payload }) {
  try {
    const { email, password } = payload;
    yield auth().signInWithEmailAndPassword(email, password);

    const user = auth().currentUser.toJSON();

    const usersRef = firestore().collection('users');

    const dataDoc = yield usersRef.doc(user.uid).get();

    const data = dataDoc.data();

    if (data.isProvider) {
      yield auth().signOut();
      yield put(signInFailure());

      Alert.alert('Ocorreu um erro');
      return;
    }

    yield put(signInSuccess({ token: user.refreshToken, user: { ...user, ...data } }));
  } catch (error) {
    if (__DEV__) {
      console.tron.display({
        name: 'SIGN IN FAILURE',
        important: true,
        value: error,
      });
    }
    yield put(signInFailure(error));

    Alert.alert('Ocorreu um erro');
  }
}

export function* signUp({ payload }) {
  try {
    const { email, password, picture, name } = payload;

    yield auth().createUserWithEmailAndPassword(email, password);
    const user = auth().currentUser;

    const pictureRef = storage().ref(
      `/media/users/${user.uid}/picture.${picture.type.split('/')[1]}`
    );

    yield pictureRef.putString(picture.data, 'base64', { contentType: picture.type });

    yield user.updateProfile({ displayName: name, photoURL: pictureRef.fullPath });

    const usersRef = firestore().collection('users');

    yield usersRef
      .doc(user.uid)
      .set({ name, email, isProvider: false, picture: pictureRef.fullPath });

    const dataDoc = yield usersRef.doc(user.uid).get();

    const data = dataDoc.data();

    yield user.reload();
    const _user = user.toJSON();

    yield put(signUpSuccess({ token: _user.refreshToken, user: { ..._user, ...data } }));
  } catch (error) {
    if (__DEV__) {
      console.tron.display({
        name: 'SIGN UP FAILURE',
        important: true,
        value: error,
      });
    }
    yield put(signUpFailure(error));

    Alert.alert('Ocorreu um erro');
  }
}

export function setToken({ payload }) {
  if (!get(payload, 'auth.token', null)) return;

  const { token } = payload.auth;

  if (token) {
    api.defaults.headers.Authorization = `${Constants.TOKE_AUTH} ${token}`;
  }
}

export function* signOut() {
  delete api.defaults.headers.Authorization;
  yield auth().signOut();
  yield put(logout());
}

export default all([
  takeLatest('persist/REHYDRATE', setToken),
  takeLatest(AuthTypes.SIGN_IN_REQUEST, signIn),
  takeLatest(AuthTypes.SIGN_UP_REQUEST, signUp),
  takeLatest(AuthTypes.LOGOUT, signOut),
]);
