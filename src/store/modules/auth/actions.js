import * as AuthTypes from './types';

/**
 *
 * @param {Object} data
 * @param {String} data.email
 * @param {String} data.password
 */
export function signInRequest(data) {
  return {
    type: AuthTypes.SIGN_IN_REQUEST,
    payload: {
      ...data,
    },
  };
}

/**
 *
 * @param {Object} data
 * @param {String} data.token
 * @param {Object} data.user
 * @param {String} data.user.name
 * @param {String} data.user.email
 * @param {String} data.user.password
 * @param {Object} data.user.picture
 */
export function signInSuccess(data) {
  return {
    type: AuthTypes.SIGN_IN_SUCCESS,
    payload: {
      ...data,
    },
  };
}

export function signInFailure(error) {
  return {
    type: AuthTypes.SIGN_IN_FAILURE,
    payload: {
      error,
    },
  };
}

/**
 *
 * @param {Object} data
 * @param {String} data.name
 * @param {String} data.email
 * @param {String} data.password
 * @param {Object} data.picture
 */
export function signUpRequest(data) {
  return {
    type: AuthTypes.SIGN_UP_REQUEST,
    payload: {
      ...data,
    },
  };
}

/**
 *
 * @param {Object} data
 * @param {String} data.token
 * @param {Object} data.user
 * @param {String} data.user.name
 * @param {String} data.user.email
 * @param {Object} data.user.picture
 */
export function signUpSuccess(data) {
  return {
    type: AuthTypes.SIGN_UP_SUCCESS,
    payload: {
      ...data,
    },
  };
}

export function signUpFailure(error) {
  return {
    type: AuthTypes.SIGN_UP_FAILURE,
    payload: {
      error,
    },
  };
}

export function logout() {
  return {
    type: AuthTypes.LOGOUT,
  };
}
