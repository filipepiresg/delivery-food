import AsyncStorage from '@react-native-community/async-storage';
import { persistReducer } from 'redux-persist';

import { Constants } from '~/commom';

export default reducers => {
  const persistedReducer = persistReducer(
    {
      key: Constants.PREFIX,
      storage: AsyncStorage,
      whitelist: ['auth', 'user'],
    },
    reducers
  );

  return persistedReducer;
};
