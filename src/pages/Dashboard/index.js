import React, { useEffect, useState } from 'react';
import { withNavigationFocus } from 'react-navigation';
import { useSelector } from 'react-redux';
import _ from 'lodash';

import {
  Container,
  SafeArea,
  List,
  FilterInput,
  FilterContainer,
  FilterContent,
  FilterButton,
  MaterialIcon,
  ProfileImage,
} from './styles';
import Establishment from './components/Establishment';
import withLoading from '~/hocs/withLoading';
import i18n from '~/i18n';

function Dashboard({ isFocused, navigation }) {
  const [establishments, setEstablishments] = useState([]);
  const [filter, setFilter] = useState('');

  const user = useSelector(state => state.user.profile);

  useEffect(() => {
    navigation.setParams({
      picture: _.get(user, 'picture', 'https://api.adorable.io/avatars/100/abott@adorable.png'),
    });
  }, [navigation, user]);

  useEffect(() => {}, [isFocused]);

  return (
    <SafeArea>
      <Container>
        <FilterContainer>
          <FilterContent>
            <FilterInput
              placeholder={i18n.t('placeholders.filter')}
              value={filter}
              onChangeText={setFilter}
              autoCapitalize='none'
              autoCorrect={false}
              keyboardType='ascii-capable'
            />
            <MaterialIcon name='search' />
          </FilterContent>
          <FilterButton onPress={() => {}}>
            <MaterialIcon name='format-list-bulleted' />
          </FilterButton>
        </FilterContainer>

        <List
          data={establishments}
          keyExtractor={item => String(item.id)}
          renderItem={({ item }) => {
            return <Establishment data={item} />;
          }}
        />
      </Container>
    </SafeArea>
  );
}

const ComponentWithLoading = withLoading(Dashboard);

ComponentWithLoading.navigationOptions = ({ navigation }) => {
  const picture = navigation.getParam('picture');
  return {
    headerTitle: 'Lanchonetes',
    headerRight: () => {
      return <ProfileImage source={{ uri: picture }} />;
    },
  };
};

export default withNavigationFocus(ComponentWithLoading);
