import styled from 'styled-components/native';
import VectorIconMaterialIcon from 'react-native-vector-icons/MaterialIcons';
import { RectButton } from 'react-native-gesture-handler';

import { colors } from '~/styles';
import fonts from '~/styles/fonts';

export const SafeArea = styled.SafeAreaView`
  flex: 1;
  background-color: ${colors.WHITE};
`;

export const Container = styled.View`
  padding: 10px 20px 0;
`;

export const FilterContainer = styled.View`
  flex-direction: row;
  align-items: center;
`;
export const FilterContent = styled.View`
  flex-direction: row;
  align-items: center;
  border: 1px solid ${colors.RED_SECONDARY};
  border-radius: 5px;
  flex: 1;
  padding-right: 10px;
`;

export const FilterInput = styled.TextInput.attrs({
  placeholderTextColor: colors.RED_SECONDARY,
  selectionTextColor: colors.RED_SECONDARY,
})`
  flex: 1;
  padding: 10px;
  color: ${colors.RED_SECONDARY};
  font-size: ${fonts.NORMAL};
  font-weight: 400;
`;

export const FilterButton = styled(RectButton).attrs({})`
  margin-left: 5px;
`;

export const MaterialIcon = styled(VectorIconMaterialIcon).attrs({
  size: 30,
  color: colors.RED_SECONDARY,
})``;

export const List = styled.FlatList.attrs({
  numColumns: 2,
  keyboardShouldPersistTaps: 'handled',
  showsVerticalScrollIndicator: false,
})`
  margin-top: 10px;
  border: 1px solid #000;
  flex: 1;
`;

export const ProfileImage = styled.Image.attrs({})`
  width: 40px;
  height: 40px;
  border-radius: 20px;
  margin: 0 20px 10px 0;
`;
