import React from 'react';
import { Platform, StyleSheet } from 'react-native';
import styled from 'styled-components/native';
import { RectButton } from 'react-native-gesture-handler';
import Svg, { Path } from 'react-native-svg';

import { colors } from '~/styles';
import fonts from '~/styles/fonts';

export const Container = styled.KeyboardAvoidingView.attrs({
  enabled: Platform.OS === 'ios',
  behavior: 'padding',
})`
  flex: 1;
  align-items: center;
  justify-content: center;
  padding: 0 20px;
`;

export const SafeArea = styled.SafeAreaView`
  flex: 1;
`;

export const Input = styled.TextInput.attrs({
  placeholderTextColor: colors.WHITE,
  selectionColor: colors.WHITE,
  underlineColorAndroid: colors.TRANSPARENT,
})`
  align-self: stretch;
  /* flex: 1; */
  padding: 10px 15px;
  color: ${colors.WHITE};
  font-size: ${fonts.NORMAL}px;
  border: 1.3px solid ${props => (props.isError ? colors.WARN : colors.WHITE)};
  margin-bottom: 16px;
  border-radius: 5px;
`;

export const Button = styled(RectButton).attrs({})`
  align-self: stretch;
  padding: 15px 0;
  border-radius: 5px;
  background-color: ${props => (props.enabled ? colors.WARN : colors.RED_SECONDARY)};
  align-items: center;
  justify-content: center;
`;

export const ButtonText = styled.Text`
  color: ${colors.RED_PRIMARY};
  font-size: ${fonts.NORMAL}px;
  font-weight: bold;
`;

export const SignInLink = styled.TouchableOpacity.attrs({
  activeOpacity: 0.7,
})`
  margin: 16px 0 10px;
  align-self: center;
`;

export const SignInText = styled.Text`
  color: ${colors.WHITE};
  font-size: ${fonts.NORMAL}px;
  text-decoration-line: underline;
  font-weight: 500;
`;

const styles = StyleSheet.create({
  svg: {
    marginBottom: 20,
  },
});

export const Logo = ({ width, height }) => (
  <Svg height={height} viewBox='0 0 64 64' width={width} style={styles.svg}>
    <Path
      d='M22 36h20v7H22zM24 16c-1.654 0-3-1.346-3-3s1.346-3 3-3 3 1.346 3 3-1.346 3-3 3zM32 16c-1.654 0-3-1.346-3-3s1.346-3 3-3 3 1.346 3 3-1.346 3-3 3zM40 16c-1.654 0-3-1.346-3-3s1.346-3 3-3 3 1.346 3 3-1.346 3-3 3z'
      fill={colors.WARN}
    />
    <Path
      d='M63 21v-2.573L34.368 1.644a4.756 4.756 0 00-4.736 0L1 18.427V21h7v27H5c-2.206 0-4 1.794-4 4s1.794 4 4 4h33.472c.739 0 1.421.397 1.736 1.012A10.961 10.961 0 0050 63h10c1.654 0 3-1.346 3-3s-1.346-3-3-3h-8a1 1 0 010-2h8c1.654 0 3-1.346 3-3s-1.346-3-3-3h-8a1 1 0 010-2h8c1.654 0 3-1.346 3-3s-1.346-3-3-3h-4V21zm-2 23a1 1 0 01-1 1h-8c-1.654 0-3 1.346-3 3s1.346 3 3 3h8a1 1 0 010 2h-8c-1.654 0-3 1.346-3 3s1.346 3 3 3h8a1 1 0 010 2H50a8.968 8.968 0 01-8.013-4.901C41.322 54.805 39.976 54 38.472 54H5c-1.103 0-2-.897-2-2s.897-2 2-2h33.472c1.504 0 2.851-.805 3.516-2.1A8.966 8.966 0 0150 43h10a1 1 0 011 1zm-20-8v6h-2v-6h-2v6h-2v-6h-2v6h-2v-6h-2v6h-2v-6h-2v6h-2v-6h-1c-2.206 0-4-1.794-4-4s1.794-4 4-4c.577 0 1.154.136 1.715.403l1.171.56.242-1.275A7 7 0 0132 22a7 7 0 016.872 5.688l.242 1.275 1.171-.56A3.973 3.973 0 0142 28c2.206 0 4 1.794 4 4s-1.794 4-4 4zm-18 8h18v1.673c-.291.415-.556.854-.792 1.314-.315.616-.997 1.013-1.736 1.013H23zm27-3c-2.613 0-5.067.914-7 2.512v-5.595A6.01 6.01 0 0048 32c0-3.309-2.691-6-6-6-.489 0-.976.063-1.453.187C39.351 22.541 35.924 20 32 20s-7.351 2.541-8.547 6.187A5.769 5.769 0 0022 26c-3.309 0-6 2.691-6 6a6.01 6.01 0 005 5.917V48H10V19H3.978L30.644 3.368a2.726 2.726 0 012.713 0L60.022 19H54v22z'
      fill={colors.WHITE}
    />
    <Path
      d='M12 44h2v2h-2zM12 36h2v2h-2zM12 40h2v2h-2zM50 27h2v2h-2zM50 19h2v2h-2zM50 23h2v2h-2z'
      fill={colors.WHITE}
    />
  </Svg>
);
