import React, { useRef, useCallback } from 'react';
import { useDispatch } from 'react-redux';
import { useFormik } from 'formik';
import * as Yup from 'yup';

import { Background } from '~/components';
import {
  Container,
  Logo as LogoSVG,
  Input,
  Button,
  ButtonText,
  SignInLink,
  SignInText,
  SafeArea,
} from './styles';
import i18n from '~/i18n';
import { signInRequest } from '~/store/modules/auth/actions';

const SignInSchema = Yup.object().shape({
  email: Yup.string()
    .email(i18n.t('errors.invalid.email'))
    .required(i18n.t('errors.empty.field')),
  password: Yup.string()
    .min(6, i18n.t('errors.empty.field'))
    .max(10, i18n.t('errors.empty.field'))
    .required(i18n.t('errors.empty.field')),
});

export default function Home({ navigation }) {
  const dispatch = useDispatch();
  const emailRef = useRef();
  const passwordRef = useRef();

  const handlerSubmit = useCallback(
    ({ email, password }) => {
      dispatch(signInRequest({ email, password }));
    },
    [dispatch]
  );

  const formik = useFormik({
    initialValues: { email: '', password: '' },
    initialErrors: { email: '', password: '' },
    onSubmit: handlerSubmit,
    validationSchema: SignInSchema,
  });

  return (
    <Background>
      <SafeArea>
        <Container>
          {/* <SvgXml width='90' height='90' xml={Logo} /> */}
          <LogoSVG width={90} height={90} />
          <Input
            ref={emailRef}
            value={formik.values.email}
            onChangeText={formik.handleChange('email')}
            autoCapitalize='none'
            autoCorrect={false}
            blurOnSubmit={false}
            onSubmitEditing={() => passwordRef.current.focus()}
            placeholder={i18n.t('placeholders.email')}
            returnKeyType='next'
            keyboardType='email-address'
            isError={formik.errors.email && formik.touched.email}
          />
          <Input
            ref={passwordRef}
            value={formik.values.password}
            onChangeText={formik.handleChange('password')}
            placeholder={i18n.t('placeholders.password')}
            secureTextEntry
            onSubmitEditing={formik.handleSubmit}
            keyboardType='ascii-capable'
            returnKeyType='send'
            isError={formik.errors.password && formik.touched.password}
          />

          <Button onPress={formik.handleSubmit} enabled={formik.isValid}>
            <ButtonText>{i18n.t('buttons.signin')}</ButtonText>
          </Button>
        </Container>

        <SignInLink onPress={() => navigation.navigate('SignUp')}>
          <SignInText>{i18n.t('labels.signup')}</SignInText>
        </SignInLink>
      </SafeArea>
    </Background>
  );
}
