import { Platform } from 'react-native';
import { RectButton } from 'react-native-gesture-handler';
import styled from 'styled-components/native';

import { colors } from '~/styles';
import fonts from '~/styles/fonts';

export const Container = styled.KeyboardAvoidingView.attrs({
  enabled: Platform.OS === 'ios',
  behavior: 'padding',
})`
  flex: 1;
  align-items: center;
  justify-content: center;
  padding: 0 20px;
`;

export const SafeArea = styled.SafeAreaView`
  flex: 1;
`;

export const ButtonPhoto = styled.TouchableOpacity.attrs({
  activeOpacity: 0.7,
})`
  width: 100px;
  height: 100px;
  border-radius: 50px;
  margin-bottom: 16px;
`;

export const Image = styled.Image.attrs({})`
  width: 100px;
  height: 100px;
  border-radius: 50px;
  border-width: 3px;
  border-color: ${colors.WHITE};
`;

export const Input = styled.TextInput.attrs({
  placeholderTextColor: colors.WHITE,
  selectionColor: colors.WHITE,
  underlineColorAndroid: colors.TRANSPARENT,
})`
  align-self: stretch;
  /* flex: 1; */
  padding: 10px 15px;
  color: ${colors.WHITE};
  font-size: ${fonts.NORMAL}px;
  border: 1.3px solid ${props => (props.isError ? colors.WARN : colors.WHITE)};
  margin-bottom: 16px;
  border-radius: 5px;
`;

export const Button = styled(RectButton).attrs({})`
  align-self: stretch;
  padding: 15px 0;
  border-radius: 5px;
  background-color: ${props => (props.enabled ? colors.WARN : colors.RED_SECONDARY)};
  align-items: center;
  justify-content: center;
`;

export const ButtonText = styled.Text`
  color: ${colors.RED_PRIMARY};
  font-size: ${fonts.NORMAL}px;
  font-weight: bold;
`;

export const SignInLink = styled.TouchableOpacity.attrs({
  activeOpacity: 0.7,
})`
  margin: 16px 0 10px;
  align-self: center;
`;

export const SignInText = styled.Text`
  color: ${colors.WHITE};
  font-size: ${fonts.NORMAL}px;
  text-decoration-line: underline;
  font-weight: 500;
`;
