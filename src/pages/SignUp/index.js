import React, { useRef, useState, useCallback } from 'react';
import { Alert } from 'react-native';
import ImagePicker from 'react-native-image-picker';
import { useDispatch } from 'react-redux';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import _ from 'lodash';

import { Background } from '~/components';
import {
  Container,
  Input,
  Button,
  ButtonText,
  SignInLink,
  SignInText,
  SafeArea,
  Image,
  ButtonPhoto,
} from './styles';
import i18n from '~/i18n';
import { signUpRequest } from '~/store/modules/auth/actions';
import withLoading from '~/hocs/withLoading';
import { Constants } from '~/commom';

const SignUpSchema = Yup.object().shape({
  name: Yup.string().required(i18n.t('errors.empty.name')),
  email: Yup.string()
    .email(i18n.t('errors.invalid.email'))
    .required(i18n.t('errors.empty.email')),
  password: Yup.string()
    .min(6, i18n.t('errors.invalid.password'))
    .max(10, i18n.t('errors.invalid.password'))
    .required(i18n.t('errors.empty.password')),
  confirmPassword: Yup.string()
    .required(i18n.t('errors.empty.field'))
    .oneOf([Yup.ref('password')]),
});

const IMAGE_PICKER_OPTIONS = {
  title: i18n.t('labels.pickerImage.title'),
  mediaType: 'photo',
  chooseFromLibraryButtonTitle: i18n.t('labels.pickerImage.chooseLibrary'),
  takePhotoButtonTitle: i18n.t('labels.pickerImage.takePicture'),
  cancelButtonTitle: i18n.t('labels.pickerImage.cancel'),
  // customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
  storageOptions: {
    skipBackup: true,
    cameraRoll: true,
    path: Constants.PREFIX,
  },
};

function SignUp({ navigation }) {
  const dispatch = useDispatch();

  const nameRef = useRef();
  const emailRef = useRef();
  const passwordRef = useRef();
  const confirmPasswordRef = useRef();

  const [picture, setPicture] = useState(null);

  const takePicture = useCallback(() => {
    ImagePicker.showImagePicker(IMAGE_PICKER_OPTIONS, response => {
      if (response.didCancel || response.error) {
        return;
      }

      setPicture(response);
    });
  }, []);

  function handlerSubmit({ name, email, password }) {
    if (!picture) {
      Alert.alert('Foto é requerida');
    } else {
      dispatch(signUpRequest({ name, email, password, picture }));
    }
  }

  const formik = useFormik({
    initialValues: { name: '', email: '', password: '', confirmPassword: '' },
    initialErrors: { name: '', email: '', password: '', confirmPassword: '' },
    onSubmit: handlerSubmit,
    validationSchema: SignUpSchema,
    enableReinitialize: true,
  });

  return (
    <Background>
      <SafeArea>
        <Container>
          <ButtonPhoto onPress={takePicture}>
            <Image
              source={{
                uri: _.get(picture, 'uri', `https://api.adorable.io/avatars/100/${Constants}.png`),
              }}
            />
          </ButtonPhoto>
          <Input
            ref={nameRef}
            value={formik.values.name}
            onChangeText={formik.handleChange('name')}
            autoCapitalize='none'
            autoCorrect={false}
            blurOnSubmit={false}
            onSubmitEditing={() => emailRef.current.focus()}
            placeholder={i18n.t('placeholders.name')}
            returnKeyType='next'
            keyboardType='ascii-capable'
            isError={formik.errors.name && formik.touched.name}
          />
          <Input
            ref={emailRef}
            value={formik.values.email}
            onChangeText={formik.handleChange('email')}
            autoCapitalize='none'
            autoCorrect={false}
            blurOnSubmit={false}
            onSubmitEditing={() => passwordRef.current.focus()}
            placeholder={i18n.t('placeholders.email')}
            returnKeyType='next'
            keyboardType='email-address'
            isError={formik.errors.email && formik.touched.email}
          />
          <Input
            ref={passwordRef}
            value={formik.values.password}
            onChangeText={formik.handleChange('password')}
            placeholder={i18n.t('placeholders.password')}
            secureTextEntry
            onSubmitEditing={() => confirmPasswordRef.current.focus()}
            keyboardType='ascii-capable'
            returnKeyType='next'
            isError={formik.errors.password && formik.touched.password}
          />
          <Input
            ref={confirmPasswordRef}
            value={formik.values.confirmPassword}
            onChangeText={formik.handleChange('confirmPassword')}
            placeholder={i18n.t('placeholders.confirmPassword')}
            secureTextEntry
            onSubmitEditing={formik.handleSubmit}
            keyboardType='ascii-capable'
            returnKeyType='send'
            isError={formik.errors.confirmPassword && formik.touched.confirmPassword}
          />

          <Button onPress={formik.handleSubmit} enabled={formik.isValid}>
            <ButtonText>{i18n.t('buttons.signup')}</ButtonText>
          </Button>
        </Container>

        <SignInLink onPress={() => navigation.navigate('SignIn')}>
          <SignInText>{i18n.t('labels.signin')}</SignInText>
        </SignInLink>
      </SafeArea>
    </Background>
  );
}

export default withLoading(SignUp);
