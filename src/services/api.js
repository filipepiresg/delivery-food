import axios from 'axios';

export const Endpoints = {
  BASE_URL: 'http://localhost:3000/',
  USER: 'users',
  SESSION: 'session',
  REGISTER: 'register',
};

const api = axios.create({
  baseURL: Endpoints.BASE_URL,
  timeout: 3000,
  headers: {
    'Content-Type': 'application/json',
    Accept: 'application/json',
  },
});

export default api;
