import { StyleSheet } from 'react-native';
import styled from 'styled-components';

import { colors } from '~/styles';

export const Container = styled.View`
  flex: 1;
`;

export const Modal = styled.View`
  ${StyleSheet.absoluteFill};
  background-color: rgba(0, 0, 0, 0.7);
  justify-content: center;
`;

export const Spinner = styled.ActivityIndicator.attrs({
  size: 'large',
  color: colors.WHITE,
})``;

export const Message = styled.Text`
  text-align: center;
  color: ${colors.WHITE};
`;
