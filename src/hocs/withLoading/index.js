import React from 'react';
import { useSelector } from 'react-redux';

import { Container, Modal, Spinner } from './styles';

const withLoading = OriginalComponent => {
  const EnhancedComponent = props => {
    const loading = useSelector(state => state.app.loading);

    return (
      <Container>
        <OriginalComponent {...props} />

        {loading && (
          <Modal>
            <Spinner />
          </Modal>
        )}
      </Container>
    );
  };

  return EnhancedComponent;
};

export default withLoading;
