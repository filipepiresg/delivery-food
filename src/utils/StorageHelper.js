import AsyncStorage from '@react-native-community/async-storage';

import { Constants } from '../commom';

const STORAGE_KEY_PREFIX = Constants.PREFIX;

/**
 * Função para setar um item no storage, **_retorna um promise_**
 * @param {String} key - Chave do item
 * @param {Object} content - Conteudo do item
 */
export const setItem = (key, content) =>
  new Promise((resolve, reject) => {
    AsyncStorage.setItem(`${STORAGE_KEY_PREFIX}:${key}`, JSON.stringify(content))
      .then(() => resolve())
      .catch(err => reject(err));
  });

/**
 * Função para setar multi itens no storage, **_retorna um promise_**
 * @param {*} content
 */
export const multiSet = content =>
  new Promise((resolve, reject) => {
    const updatedContent = content.map(item => {
      item[0] = `${STORAGE_KEY_PREFIX}:${item[0]}`;
      item[1] = typeof item[1] === 'object' ? JSON.stringify(item[1]) : item[1];

      return item;
    });

    AsyncStorage.multiSet(updatedContent)
      .then(() => resolve())
      .catch(err => reject(err));
  });

/**
 * Função para get de um item no storage, **_retorna um promise_**
 * @param {String} key - Chave do item a ser pego
 */
export const getItem = key =>
  new Promise((resolve, reject) => {
    AsyncStorage.getItem(`${STORAGE_KEY_PREFIX}:${key}`)
      .then(value => resolve(value))
      .catch(err => reject(err));
  });

/**
 * Função para remover um item do storage, **_retorna um promise_**
 * @param {String} key - Chave do item a ser removido
 */
export const removeItem = key =>
  new Promise((resolve, reject) => {
    AsyncStorage.removeItem(`${STORAGE_KEY_PREFIX}:${key}`)
      .then(() => resolve())
      .catch(err => reject(err));
  });

/**
 * Função para limpar todo o storage da aplicação, **_retorna um promise_**
 */
export const clear = () =>
  new Promise((resolve, reject) => {
    AsyncStorage.clear()
      .then(() => resolve())
      .catch(err => reject(err));
  });

/**
 * Função para pegar o token do usuario salvo na aplicacao, **_retorna um promise_**
 */
export const getAccessToken = async () => JSON.parse(await getItem('token'));
/**
 * Função para pegar o token do usuario salvo na aplicacao, **_retorna um promise_**
 * @param {any} token - Token a ser salvo no storage
 */
export const saveAccessToken = token => setItem('token', token);
