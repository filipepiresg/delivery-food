import { format, parse } from 'date-fns';

import i18n from '~/i18n';

/**
 * Removes mask from given cpf
 *
 * @param {string} cpf masked cpf
 * @returns {string} unmasked cpf
 * @example
 * unmaskCPF('123.456.789.00')
 * '12345678900'
 */
export function unmaskCPF(cpf) {
  return cpf.replace(/\D/g, '');
}

/**
 * Parses date string from pt-BR format to database format
 *
 * @param {string} date pt-BR date
 * @param {string} from current format
 * @param {string} to expected format
 * @example
 * formatDate('20/12/2019')
 * '2019-12-20'
 */
export function formatDate(date, from = 'dd/MM/yyyy', to = 'yyyy-MM-dd') {
  return format(parse(date, from, new Date()), to);
}

/**
 * Formats number to currency format
 *
 * @param {number | string} value value to be formatted
 * @example
 * toCurrency(15)
 * 'R$ 15,00'
 */
export function toCurrency(value) {
  return i18n.toCurrency(Number(value), {
    precision: 2,
    delimiter: '.',
    separator: ',',
    unit: 'R$ ',
  });
}

/**
 * Capitalize the first letter of a given string
 *
 * @param {string} word Word to be capitalized
 * @example
 * capitalize('mastercard')
 * 'Mastercard'
 */
export function capitalize(word) {
  if (typeof word !== 'string') {
    return '';
  }

  return word.charAt(0).toUpperCase() + word.slice(1);
}

/**
 * Obfuscate credit card number to hide sensitive data
 *
 * @param {string} lastDigits last digits from credit card number
 * @example
 * obfuscateCreditCard('1234')
 * '**** **** **** 1234'
 */
export function obfuscateCreditCard(lastDigits) {
  if (!lastDigits) {
    return '';
  }

  return `**** **** **** ${lastDigits}`;
}
