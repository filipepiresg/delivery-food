import React from 'react';
import createAnimatedSwitchNavigator from 'react-navigation-animated-switch';
import { Transition } from 'react-native-reanimated';
import { createAppContainer } from 'react-navigation';

import AppNavigator from './Navigators/AppNavigator';
import AuthNavigator from './Navigators/AuthNavigator';

export default (isSigned = false) =>
  createAppContainer(
    createAnimatedSwitchNavigator(
      {
        Auth: AuthNavigator,
        App: AppNavigator,
      },
      {
        transition: (
          <Transition.Together>
            <Transition.Out type='slide-bottom' durationMs={400} interpolation='easeIn' />
            <Transition.In type='fade' durationMs={500} />
          </Transition.Together>
        ),
        initialRouteName: isSigned ? 'App' : 'Auth',
      }
    )
  );
