import { createStackNavigator } from 'react-navigation-stack';

import { colors } from '~/styles';
import { DashboardPage } from '~/pages';

export default createStackNavigator(
  {
    DashboardPage,
  },
  {
    defaultNavigationOptions: {
      headerTitleStyle: {
        color: colors.WHITE,
        fontWeight: '500',
        fontFamily: 'Cocogoose',
      },
      headerStyle: {
        backgroundColor: colors.RED_SECONDARY,
      },
    },
  }
);
