import { createSwitchNavigator } from 'react-navigation';

import { SignInPage, SignUpPage } from '~/pages';

export default createSwitchNavigator({
  SignIn: {
    screen: SignInPage,
  },
  SignUp: {
    screen: SignUpPage,
  },
});
