import React from 'react';
import { StatusBar } from 'react-native';
import { useSelector } from 'react-redux';

import { createRouter, NavigationService } from '~/routes';
import { colors } from './styles';
import withLoading from './hocs/withLoading';

function App() {
  const isSigned = useSelector(state => state.auth.signed);
  const Router = createRouter(isSigned);

  return (
    <>
      <StatusBar barStyle='light-content' backgroundColor={colors.RED_PRIMARY} />
      <Router
        ref={navigatorRef => {
          NavigationService.setTopLevelNavigator(navigatorRef);
        }}
      />
    </>
  );
}

export default withLoading(App);
